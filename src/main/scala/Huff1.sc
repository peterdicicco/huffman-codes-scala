abstract class CodeTree
case class Fork(left: CodeTree, right: CodeTree, chars: List[Char], weight: Int) extends CodeTree
case class Leaf(char: Char, weight: Int) extends CodeTree

type Bit = Int


val lc = List('x','x','x','x','t','t','e')
val ct = createCodeTree(lc)

val bts:List[Bit] = List(1,0,1,0,0,1,1,0,1)

val msg = decode(ct,bts)

val bts2 = encode(ct)(msg)


val frenchCode: CodeTree = Fork(Fork(Fork(Leaf('s',121895),Fork(Leaf('d',56269),Fork(Fork(Fork(Leaf('x',5928),Leaf('j',8351),List('x','j'),14279),Leaf('f',16351),List('x','j','f'),30630),Fork(Fork(Fork(Fork(Leaf('z',2093),Fork(Leaf('k',745),Leaf('w',1747),List('k','w'),2492),List('z','k','w'),4585),Leaf('y',4725),List('z','k','w','y'),9310),Leaf('h',11298),List('z','k','w','y','h'),20608),Leaf('q',20889),List('z','k','w','y','h','q'),41497),List('x','j','f','z','k','w','y','h','q'),72127),List('d','x','j','f','z','k','w','y','h','q'),128396),List('s','d','x','j','f','z','k','w','y','h','q'),250291),Fork(Fork(Leaf('o',82762),Leaf('l',83668),List('o','l'),166430),Fork(Fork(Leaf('m',45521),Leaf('p',46335),List('m','p'),91856),Leaf('u',96785),List('m','p','u'),188641),List('o','l','m','p','u'),355071),List('s','d','x','j','f','z','k','w','y','h','q','o','l','m','p','u'),605362),Fork(Fork(Fork(Leaf('r',100500),Fork(Leaf('c',50003),Fork(Leaf('v',24975),Fork(Leaf('g',13288),Leaf('b',13822),List('g','b'),27110),List('v','g','b'),52085),List('c','v','g','b'),102088),List('r','c','v','g','b'),202588),Fork(Leaf('n',108812),Leaf('t',111103),List('n','t'),219915),List('r','c','v','g','b','n','t'),422503),Fork(Leaf('e',225947),Fork(Leaf('i',115465),Leaf('a',117110),List('i','a'),232575),List('e','i','a'),458522),List('r','c','v','g','b','n','t','e','i','a'),881025),List('s','d','x','j','f','z','k','w','y','h','q','o','l','m','p','u','r','c','v','g','b','n','t','e','i','a'),1486387)


val secret: List[Bit] = List(0,0,1,1,1,0,1,0,1,1,1,0,0,1,1,0,1,0,0,1,1,0,1,0,1,1,0,0,1,1,1,1,1,0,1,0,1,1,0,0,0,0,1,0,1,1,1,0,0,1,0,0,1,0,0,0,1,0,0,0,1,0,1)

val english = decode(frenchCode,secret)

encode(frenchCode)(english)





def encode(tree: CodeTree)(text: List[Char]): List[Bit] = {
  def _encode(tree: CodeTree , text: List[Char] , acc:List[Bit]): List[Bit] = {
    if (text.isEmpty) acc.reverse
    else {
      val nextbits = getNextBits(tree:CodeTree , text: List[Char])
      _encode(tree , nextbits._2 , acc ::: nextbits._1  )
    }
  }
  val x:List[Bit] = Nil
  _encode(tree,text,x)
}

def getNextBits(tree:CodeTree , text: List[Char] ):(List[Bit],List[Char]) = {
  def _getNextBits(tree: CodeTree, text: List[Char], acc: List[Bit]): (List[Bit], List[Char]) = {
    tree match {
      case tree: Leaf => (acc, text.tail)
      case tree: Fork => {
        val l = tree.left
        l match {
          case l:Leaf => if (l.char == text.head)  _getNextBits(tree.left,text, acc ::: List(0)) else _getNextBits(tree.right,text, acc ::: List(1))
          case l:Fork => if (l.chars.contains(text.head)) _getNextBits(tree.left,text, acc ::: List(0)) else _getNextBits(tree.right,text, acc ::: List(1))
        }
      }
    }
  }
  val x: List[Bit] = Nil
  _getNextBits(tree, text, x)
}

def decode(tree: CodeTree, bits: List[Bit]): List[Char] = {
  def _decode(tree:CodeTree , bits: List[Bit] , acc:List[Char]): List[Char] = {
    if (bits.isEmpty) acc.reverse
    else {
      val nextlet = getNextLetter(tree:CodeTree , bits: List[Bit])
      _decode(tree , nextlet._2 , nextlet._1 :: acc  )
    }
  }
  val x:List[Char] = Nil
  _decode(tree,bits,x)
}

def getNextLetter(tree:CodeTree , bits: List[Bit] ):(Char,List[Bit]) = {
  tree match {
    case tree : Leaf => (tree.char , bits )
    case tree : Fork =>
      if (bits.head == 0) getNextLetter(tree.left , bits.tail )
      else getNextLetter(tree.right , bits.tail )

  }
}

def createCodeTree(chars: List[Char]): CodeTree = {
  val z2 = makeOrderedLeafList(times(chars))
  val z3 = until(singleton, combine)(z2)
  z3.head
}

def combine(trees: List[CodeTree]): List[CodeTree] = {
  if (singleton(trees)) trees
  else {
    val s1a = makeCodeTree(trees.head,trees.tail.head)
    val s1b = trees.tail.tail
    val s1c:List[CodeTree] = s1a :: s1b
    s1c.sortBy(x=>weight(x))
  }
}

def until(isSingleTon: List[CodeTree]=>Boolean, comb: List[CodeTree]=>List[CodeTree])(trees: List[CodeTree]): List[CodeTree] = {
  if (isSingleTon(trees)) trees
  else {
    val s1 = comb(trees)
    until(isSingleTon,comb)(s1)
  }
}

def weight(tree: CodeTree): Int = tree match {
  case tree : Fork => tree.weight
  case tree : Leaf => tree.weight
  case _ => 0
}

def chars(tree: CodeTree): List[Char] = tree match {
  case tree : Fork => tree.chars
  case tree : Leaf => List(tree.char)
  case _ => Nil
}

def makeCodeTree(left: CodeTree, right: CodeTree) = Fork(left, right, chars(left) ::: chars(right), weight(left) + weight(right))
def string2Chars(str: String): List[Char] = str.toList
def times(chars: List[Char]): List[(Char, Int)] = chars.groupBy(x=>x).map(a => (a._1,a._2.length)).toList
def makeOrderedLeafList(freqs: List[(Char, Int)]): List[Leaf] = freqs.sortBy(a=>a._2).map(i => Leaf(i._1,i._2))
def singleton(trees: List[CodeTree]): Boolean = trees.length==1


